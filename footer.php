<?php 
/**
 * 页面底部信息
 */
if(!defined('EMLOG_ROOT')) {exit('error!');} 
?>
<div class="clear"></div>

	<footer class="footer">
		
		Powered by <a href="http://www.emlog.net" title="采用emlog系统">emlog</a>丨
		Theme BY <a href="http://www.im050.com" title='MEMORY'>MEMORY</a>丨
		Modify BY <a href="http://www.jermey.cn" title='四少爷'>四少爷</a>丨
		<a href="http://www.miibeian.gov.cn" target="_blank"><?php echo $icp; ?></a>丨 
		<?php echo $footer_info; ?>
		<?php doAction('index_footer'); ?>
		
	</footer>

</div>
<canvas class="pjax_loading" id="Loading"></canvas>
<script>prettyPrint();</script>

<!--调用Loading.Js-->
<script src="<?php echo TEMPLATE_URL; ?>js/Loading.js" type="text/javascript"></script>
<script type="text/javascript">
$(window).resize(function(){$.getScript("<?php echo TEMPLATE_URL; ?>js/Loading.js");});
</script>

<script>
	//文章页图片自适应
	function responsiveImg() {
		var img_count=(jQuery('.article .content').find('img')).length;
		if (img_count != 0) {
		var maxwidth=jQuery(".article .content").width();
		for (var i=0;i<=img_count-1;i++) {
			var max_width=jQuery('.article .content img:eq('+i+')');
				if (max_width.width() > maxwidth) {
					max_width.addClass('responsive-img');
				}
			}
		}
	}
	jQuery(function(){
		responsiveImg();
		window.onresize = function(){
			responsiveImg();
		}
	});
</script>

<script type="text/javascript">
/* <![CDATA[ */
var mejsL10n = {"language":"zh-CN","strings":{"Close":"\u5173\u95ed","Fullscreen":"\u5168\u5c4f","Download File":"\u4e0b\u8f7d\u6587\u4ef6","Download Video":"\u4e0b\u8f7d\u89c6\u9891","Play\/Pause":"\u64ad\u653e\/\u6682\u505c","Mute Toggle":"\u5207\u6362\u9759\u97f3","None":"\u65e0","Turn off Fullscreen":"\u5173\u95ed\u5168\u5c4f","Go Fullscreen":"\u5168\u5c4f","Unmute":"\u53d6\u6d88\u9759\u97f3","Mute":"\u9759\u97f3","Captions\/Subtitles":"\u5b57\u5e55"}};
var _wpmejsSettings = {"pluginPath":"\/wp\/wp-includes\/js\/mediaelement\/"};
/* ]]> */
</script>

<!-- 多说公共JS代码 start (一个网页只需插入一次) -->
<script type="text/javascript">
var duoshuoQuery = {short_name:"<?php echo _g('duoshuo');?>"};
	(function() {
		var ds = document.createElement('script');
		ds.type = 'text/javascript';ds.async = true;
		ds.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//static.duoshuo.com/embed.js';
		ds.charset = 'UTF-8';
		(document.getElementsByTagName('head')[0] 
		 || document.getElementsByTagName('body')[0]).appendChild(ds);
	})();
</script>
<!-- 多说公共JS代码 end -->
<script  type="text/javascript">
$(document).pjax('a[target!=_blank]', '#article-list', {fragment: '#article-list',timeout: 6000}); 
$(document).on('submit', 'form', function (event) {$.pjax.submit(event, '#article-list', {fragment:'#article-list', timeout:6000});}); 
</script>
<script>
$(document).on('pjax:send', function() { //pjax链接点击后显示加载动画；
    $(".pjax_loading").css("display", "block");
});
$(document).on('pjax:complete', function() { //pjax链接加载完成后隐藏加载动画；
    $(".pjax_loading").css("display", "none");
	$(".list").stop().animate({left: 110}, 500);
	$(".icon-category").removeClass('list-open').children('span').html('展开分类目录');
	if ( $('.ds-thread').length > 0 ) { 
    if (typeof DUOSHUO !== 'undefined') DUOSHUO.EmbedThread('.ds-thread');
    else $.getScript("//static.duoshuo.com/embed.js");
 }
});
</script>
</body>
</html>
    