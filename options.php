<?php





/*@support tpl_options*/


!defined('EMLOG_ROOT') && exit('access deined!');


$options = array(


		//主题公告	


		'duoshuo' => array(


	    'type' => 'text',


		'name' => '多说域名',


		'description' => '在多说创建账号时的域名；比如：http://test.duoshuo.com中的test',


		'default' => 'test',


		),


	


		'logo' => array(


        'type' => 'image',


        'name' => '左侧头像',


        'values' => array(


            TEMPLATE_URL . 'images/face.png',


        	),


		'description' => '站点左侧头像，建议png格式，大小正方形。不能上传请手动ftp',


    	),


	


	


		'qq' => array(


	    'type' => 'text',


		'name' => '站长qq',


		'default' => '993745241',


		),


	


		'weixin' => array(


	    'type' => 'text',


		'name' => '微信号',


		'default' => 'VIP_LPJ',


		),


	


		'facename' => array(


	    'type' => 'text',


		'name' => '站长贵姓',


		'default' => '	刘木头',


		),


	


		'dis_num' => array(


		'type' => 'text',


		'name' => '自动摘要字符数',


		'description' => '请根据需要输入整数以控制首页摘要的字符数量',


		'default' => '490',


		),


	


		'slt' => array(


	    'type' => 'radio',


		'name' => 'TimThumb缩略图神器',


	    'description' => '如果使用，到模版目录中cache权限设置777，如果不使用可能导致图片变形严重，建议在从别的地方扒文章时将图片另行保存再重新上传',


		'values' => array(


			'yes' => '使用',


			'no' => '禁用',


		),


		'default' => 'no',


		),


	


		'css' => array(


	    'type' => 'radio',


		'name' => 'css样式',


	    'description' => '选择博客模板样式，红橙绿',


		'values' => array(


			'red' => '红',


			'orange' => '橙',


			'green' => '绿',


		),


		'default' => 'red',


		),


);