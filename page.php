<?php 
/**
 * 自建页面模板
 */
if(!defined('EMLOG_ROOT')) {exit('error!');} 
?>
<div class="container">
	<div class="article-list" id="article-list">
		<article class="article">
			<h1><a><?php echo $log_title; ?></a></h1>
			
			<div class="content">
			<p><?php echo $log_content; ?></p>	
			</div>
			
			<div class="article-info">
			</div>
		</article>
		
		<section class="comments">
		<?php blog_comments($comments,$comnum); ?>
		<?php blog_comments_post($logid,$ckname,$ckmail,$ckurl,$verifyCode,$allow_remark); ?>
		</section>
		
		
	</div>
<?php include View::getView('side');?>
<?php include View::getView('footer');?>