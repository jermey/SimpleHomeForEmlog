<?php
/*
Template Name:SimpleHome
Description:主题原作为：Memory；由四少爷进行修改。整理页面DIV标签。整合多说。修改手机版菜单。三栏,响应式,文章置顶,特色图像,无障碍友好,红色,博客,分享,扁平化
Version:2.0
Author:四少爷
Author Url:http://www.jermey.cn
Sidebar Amount:1
License:GNU General Public License v2.0
License URI:http://www.gnu.org/licenses/gpl-2.0.html
*/

if(!defined('EMLOG_ROOT')) 
{
	exit('error!');
}
require_once View::getView('module');
?>
<!DOCTYPE html>
<!--/**
 * 　　　　　　　　┏┓　　　┏┓
 * 　　　　　　　┏┛┻━━━┛┻┓
 * 　　　　　　　┃　　　　　　　┃ 　
 * 　　　　　　　┃　　　━　　　┃
 * 　　　　　　　┃　＞　　　＜　┃
 * 　　　　　　　┃　　　　　　　┃
 * 　　　　　　　┃...　⌒　...　┃
 * 　　　　　　　┃　　　　　　　┃
 * 　　　　　　　┗━┓　　　┏━┛
 * 　　　　　　　　　┃　　　┃　Code is far away from bug with the animal protecting　　　　　　　　　　
 * 　　　　　　　　　┃　　　┃   神兽保佑,代码无bug
 * 　　　　　　　　　┃　　　┃　　　　　　　　　　　
 * 　　　　　　　　　┃　　　┃  　　　　　　
 * 　　　　　　　　　┃　　　┃
 * 　　　　　　　　　┃　　　┃　　　　　　　　　　　
 * 　　　　　　　　　┃　　　┗━━━┓
 * 　　　　　　　　　┃　　　　　　　┣┓
 * 　　　　　　　　　┃　　　　　　　┏┛
 * 　　　　　　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　　　　　　┃┫┫　┃┫┫
 * 　　　　　　　　　　┗┻┛　┗┻┛
 */
  -->
<html>
<head>
<title>
<?php
if($site_title=='')
{
	echo "很抱歉，没有找到你想要的内容";
}
else
{
	echo $site_title;
}
?>
</title>
<meta name="baidu-site-verification" content="W3VjrDC2im" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta name="format-detection" content="telephone=no"/>
<meta name="apple-mobile-app-status-bar-style" content="black" />
<meta name="apple-touch-fullscreen" content="YES" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, initial-scale=1.0,  minimum-scale=1.0, maximum-scale=1.0" />
<meta name="keywords" content="<?php echo $site_key; ?>" />
<meta name="description" content="<?php echo $site_description; ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="emlog" />
<script type="text/javascript" src="<?php echo TEMPLATE_URL; ?>scripts/js/jquery.js?ver=1.11.0"></script>
<script src="<?php echo BLOG_URL; ?>include/lib/js/common_tpl.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo TEMPLATE_URL; ?>scripts/js/jquery-migrate.min.js?ver=1.2.1"></script>
<script src="<?php echo TEMPLATE_URL; ?>scripts/highlight.pack.js"></script>
<script src="<?php echo TEMPLATE_URL; ?>js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="<?php echo TEMPLATE_URL; ?>js/jquery.login.js" type="text/javascript"></script>
<script src="<?php echo TEMPLATE_URL; ?>js/jquery.pjax.js" type="text/javascript"></script>
<script src="<?php echo BLOG_URL; ?>admin/editor/plugins/code/prettify.js" type="text/javascript"></script>
<script src="<?php echo TEMPLATE_URL; ?>scripts/jquery.poshytip.min.js?ver=1.2"></script>
<script src="<?php echo TEMPLATE_URL; ?>scripts/custom.js?ver=1.0"></script>
<script type="text/javascript" src="<?php echo TEMPLATE_URL; ?>comments-ajax.js?ver=3.9.2"></script>
<script type="text/javascript">
if (document.domain != '<?php echo substr(BLOG_URL,7,-1); ?>'){
 window.location.href='<?php echo substr(BLOG_URL,0,-1); ?>';
}
</script>
	<!--[if lte IE 8]>
	<script type="text/javascript" src="<?php echo TEMPLATE_URL; ?>scripts/html5.js"></script>
    <script type="text/javascript" src="<?php echo TEMPLATE_URL; ?>scripts/css3-mediaqueries.js"></script>
<![endif]-->
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="<?php echo BLOG_URL; ?>xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?php echo BLOG_URL; ?>wlwmanifest.xml" />
<link rel="alternate" type="application/rss+xml" title="RSS"  href="<?php echo BLOG_URL; ?>rss.php" />
<link href="<?php echo TEMPLATE_URL; ?>css/tip-twitter/tip-twitter.css?ver=1.2" rel="stylesheet">
<link href="<?php echo TEMPLATE_URL; ?>css/font-awesome.min.css?ver=4.1.0" rel="stylesheet">
<link href="<?php echo TEMPLATE_URL; ?>css/login.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo TEMPLATE_URL; ?>css/tomorrow.css">
<link rel="stylesheet" id="style-css" href="<?php echo TEMPLATE_URL; ?>style.css?ver=2.0" type="text/css" media="">
<link rel="stylesheet" id="skincolor-css" href="<?php echo TEMPLATE_URL; ?>css/skin/skin-<?php echo _g('css'); ?>.css" type="text/css" media="">
<link href="<?php echo BLOG_URL; ?>admin/editor/plugins/code/prettify.css" rel="stylesheet" type="text/css" />
<?php doAction('index_head'); ?>
</head>

<body>
	
<!--loading-->
<div class="loading" style="width: 100%; display: none;"></div>
	
<div class="circle-loading" style="display: none;"></div>
	
<!-- float category -->
<div class="list">
	<?php navi_sort();?>
</div>
	
	
<header class="left">
	<div id="categories">
		<i class="fa fa-bars"></i>
		<?php blog_small_sort();?>
	</div>
	
	<div class="sns-icon">
		<ul>
			<li class="sns-qq"><span>QQ:<?php echo _g('qq');?>
			</span></li>
			<li class="sns-weichat"><span>微信:<?php echo _g('weixin');?>
			</span></li>
			<li class="icon-category"><span>展开分类目录</span></li>
		</ul>
	</div>
	
	<div class="to-top">
		<i class="fa fa-angle-up"></i>
	</div>
	
	<div class="face-area">
		<div class="face-img">
			<img src="<?php echo _g('logo'); ?>" width="auto" height="100%" />
		</div>
		<div class="face-name">
			<?php echo _g('facename');?>
		</div>
	</div>
	
	<div class="search">
		<form method="get" id="searchform" class="searchform" action="<?php echo BLOG_URL; ?>
			index.php" role="search">
			<input type="text" name="keyword" id="s" placeholder="Your Keywords">
			<input type="submit" value="">
		</form>
	</div>
	
	<div class="nav">
		<ul>
			<?php blog_navi();?>
			<li id="nav-current" class="nav-current" style="top: 0px;"></li>
		</ul>
	</div>
</header>