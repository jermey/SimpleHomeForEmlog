<?php 
/**
 * 侧边栏组件、页面模块
 */
if(!defined('EMLOG_ROOT')) {exit('error!');} 
if (!function_exists('_g')) {
	emMsg('请先安装<a href="https://github.com/Baiqiang/em_tpl_options" target="_blank">模板设置插件</a>', BLOG_URL . 'admin/plugins.php');
}
?>
<?php
//widget：blogger
function widget_blogger($title){
	global $CACHE;
	$user_cache = $CACHE->readCache('user');
	$name = $user_cache[1]['mail'] != '' ? "<a href=\"mailto:".$user_cache[1]['mail']."\" class='upp'>".$user_cache[1]['name']."</a>" : $user_cache[1]['name'];?>
<?php }?>
<?php
//widget：日历
function widget_calendar($title){ ?>
<?php }?>
<?php
//widget：标签
function widget_tag($title){
	global $CACHE;
	$tag_cache = $CACHE->readCache('tags');?>
	<div id="tag_cloud-3" class="widget sidebox widget_tag_cloud" style=""><i class="fa fa-caret-down"></i>
        <h2><?php echo $title; ?></h2>
     <div class="tagcloud">
		<?php 
		shuffle ($tag_cache);
		 $tag_cache = array_slice($tag_cache,0,43);
		foreach($tag_cache as $value): ?>
        <a  href="<?php echo Url::tag($value['tagurl']); ?>"  class="tag-link-<?php echo rand(12, 49) ?>" style="font-size: 8pt;" title="<?php echo $value['usenum']; ?>个话题"><?php echo $value['tagname']; ?></a><?php endforeach; ?>
         </div>
    </div>
<?php }?>
<?php
//widget：分类
function widget_sort($title){
	global $CACHE;
	$sort_cache = $CACHE->readCache('sort'); ?>
<div id="categories" class="widget sidebox widget_categories">				<i class="fa fa-caret-down"></i>
        <h2><?php echo $title; ?></h2>
        <ul class="list-group zan-category">
       <li class="cat-item cat-item">	
	   <?php foreach($sort_cache as $value):if ($value['pid'] != 0) continue;?><a href="<?php echo Url::sort($value['sid']); ?>"><?php echo $value['sortname']; ?>(<?php echo $value['lognum'] ?>)</a><?php endforeach; ?></li>
    </ul>
      </div>
<?php }?>
<?php
//widget：最新微语
function widget_twitter($title){
	global $CACHE; 
	$newtws_cache = $CACHE->readCache('newtw');
	$istwitter = Option::get('istwitter');
	
	?>
<div id="recent-comments-4" class="widget sidebox widget_recent_comments"><i class="fa fa-caret-down"></i><h2><?php echo $title; ?></h2>
          <ul id="recentcomments">
    <?php foreach($newtws_cache as $value):?>
    <li class="recentcomments">
    <a href="<?php echo BLOG_URL . 't/'; ?>"><?php echo $value['t']; ?><?php echo $img;?></a>
    </li>
    <?php endforeach; ?>
    </ul>
        </div>
<?php }?>
<?php
//widget：最新评论
function widget_newcomm($title){
	global $CACHE;
	$com_cache = $CACHE->readCache('comment');
	?>
<div id="recent-comments" class="widget sidebox widget_recent_comments"><i class="fa fa-caret-down"></i>
        <h2><?php echo $title; ?></h2>
          <ul id="recentcomments">
		<?php
		foreach($com_cache as $value):
		$articleUrl = Url::log($value['gid']);
		$url = Url::comment($value['gid'], $value['page'], $value['cid']);
		$db = MySql::getInstance();
		$sql = "SELECT title FROM ".DB_PREFIX."blog WHERE gid=".$value['gid'];
		$ret = $db->query($sql);
		$row = $db->fetch_array($ret);
		$articleTitle = $row['title'];
		$db = MySql::getInstance();
		$sql = "SELECT url FROM ".DB_PREFIX."comment WHERE cid=".$value['cid'];
		$ret = $db->query($sql);
		$row = $db->fetch_array($ret);
		?>
		<li class="recentcomments">
<?php echo $value['name']; ?>评论在《
<a href="<?php echo $articleUrl; ?>" title="ta说道：<?php echo $value['content']; ?>"><?php echo $articleTitle; ?></a>》
        </li>
		<?php endforeach; ?>
		</ul>
      </div>
<?php }?>
<?php
//widget：最新文章
function widget_newlog($title){
	global $CACHE; 
	$newLogs_cache = $CACHE->readCache('newlog');
	?>
   <div id="recent-posts-6" class="widget sidebox widget_recent_entries">		<i class="fa fa-caret-down"></i>
        <h2><?php echo $title; ?></h2>
          <ul>
	<?php foreach($newLogs_cache as $value): ?> 
    <li>
    <a href="<?php echo Url::log($value['gid']); ?>" title="<?php echo $value['title']; ?>"><?php echo mb_substr($value['title'],0,50,'utf8');?></a></li><?php endforeach; ?>
    </ul>
    </div>
<?php }?>
<?php
//widget：热门文章
function widget_hotlog($title){
	$index_hotlognum = Option::get('index_hotlognum');
	$Log_Model = new Log_Model();
	$randLogs = $Log_Model->getHotLog($index_hotlognum);?>
 <div id="recent-posts-6" class="widget sidebox widget_recent_entries">		<i class="fa fa-caret-down"></i>
        <h2><?php echo $title; ?></h2>

          <ul>
<?php foreach($randLogs as $value): ?>
<li><a href="<?php echo Url::log($value['gid']); ?>" title="<?php echo $value['title']; ?>"><?php echo mb_substr($value['title'],0,50,'utf8');?></a></li><?php endforeach; ?>
</ul>
 </div>
<?php }?>
<?php
//widget：随机文章
function widget_random_log($title){
	$index_randlognum = Option::get('index_randlognum');
	$Log_Model = new Log_Model();
	$randLogs = $Log_Model->getRandLog($index_randlognum);?>
	 <div id="recent-posts-6" class="widget sidebox widget_recent_entries">		<i class="fa fa-caret-down"></i>
        <h2><?php echo $title; ?></h2>
          <ul>
		  <?php foreach($randLogs as $value): ?>
          <li><a href="<?php echo Url::log($value['gid']); ?>" title="<?php echo $value['title']; ?>"><?php echo mb_substr($value['title'],0,50,'utf8');?></a></li><?php endforeach; ?>
</ul>
 </div>
<?php }?>
<?php
//widget：搜索
function widget_search($title){ ?>
      <div id="search-4" class="widget sidebox widget_search">
       <form class="searchform">
			<input name="keyword" id="s" type="text" class="form-control" placeholder="搜索轻轻点这里输入按回车哦~"  style="width: 174px;padding: 0 36px 0 10px;height: 30px;border: none;color: #666;background: #fff;outline: none;border-radius: 15px;">
		</form>
    </div>
<?php } ?>
<?php
//widget：归档
function widget_archive($title){
	global $CACHE; 
	$record_cache = $CACHE->readCache('record');
	?>
    <div id="archives-4" class="widget sidebox widget_archive">
    <i class="fa fa-caret-down"></i>
        <h2><?php echo $title; ?></h2>
        <ul><li>
		<?php foreach($record_cache as $value): ?><a href="<?php echo Url::record($value['date']); ?>"><?php echo $value['record']; ?>(<?php echo $value['lognum']; ?>)</a><?php endforeach; ?></li></ul>
    </div>

<?php } ?>
<?php
//widget：自定义组件
function widget_custom_text($title, $content){?>
<div id="recent-posts" class="widget sidebox widget_recent_entries">
        <h2><?php echo $title; ?></h2>
        <ul>
	<?php echo $content; ?>
	</ul>
    </div>
<?php } ?>
<?php
//widget：链接
function widget_link($title){
	global $CACHE; 
	$link_cache = $CACHE->readCache('link');
    //if (!blog_tool_ishome()) return;#只在首页显示友链去掉双斜杠注释即可
	?>
	<div id="tag_cloud-3" class="widget sidebox widget_tag_cloud" style=""><i class="fa fa-caret-down"></i>
        <h2><?php echo $title; ?></h2>
       <div class="tagcloud">
	<?php foreach($link_cache as $value): ?>
	<a href="<?php echo $value['url']; ?>" title="<?php echo $value['des']; ?>" target="_blank"><?php echo $value['link']; ?></a>
	<?php endforeach; ?>
	</div>
      </div>
<?php }?> 

<?php
//blog：导航
function blog_navi(){
	global $CACHE; 
	$navi_cache = $CACHE->readCache('navi');
	?>
	<ul>
	<?php
	foreach($navi_cache as $value):

        if ($value['pid'] != 0) {
            continue;
        }

		if($value['url'] == ROLE_ADMIN && (ROLE == ROLE_ADMIN || ROLE == ROLE_WRITER)):
			?>
			<li id="menu-item-111" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-111"><a href="<?php echo BLOG_URL; ?>admin/">管理站点</a></li>
			<li id="menu-item-112" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-112"><a href="<?php echo BLOG_URL; ?>admin/?action=logout">退出</a></li>
			<?php 
			continue;
		endif;
		$newtab = $value['newtab'] == 'y' ? 'target="_blank"' : '';
        $value['url'] = $value['isdefault'] == 'y' ? BLOG_URL . $value['url'] : trim($value['url'], '/');
        $current_tab = BLOG_URL . trim(Dispatcher::setPath(), '/') == $value['url'] ? 'current' : 'common';
		?>
		<li id="menu-item-<?php echo $current_tab;?>" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-<?php echo $current_tab;?>">
			<a href="<?php echo $value['url']; ?>" <?php echo $newtab;?>><?php echo $value['naviname']; ?></a>
		</li>
	<?php endforeach; ?>
	</ul>
<?php }?>

<?php
//blog：small导航
function blog_small_navi(){
	global $CACHE; 
	$navi_cache = $CACHE->readCache('navi');
	?>
	<select>
    <option value="<?php echo BLOG_URL; ?>">请选择</option>
	<?php
	foreach($navi_cache as $value):

        if ($value['pid'] != 0) {
            continue;
        }
		if($value['url'] == ROLE_ADMIN && (ROLE == ROLE_ADMIN || ROLE == ROLE_WRITER)):
			?>
			<option value="<?php echo BLOG_URL; ?>admin/">管理站点</option>
			<option value="<?php echo BLOG_URL; ?>admin/?action=logout">退出</option>
			<?php 
			continue;
		endif;
		$newtab = $value['newtab'] == 'y' ? 'target="_blank"' : '';
        $value['url'] = $value['isdefault'] == 'y' ? BLOG_URL . $value['url'] : trim($value['url'], '/');
        $current_tab = BLOG_URL . trim(Dispatcher::setPath(), '/') == $value['url'] ? 'current' : 'common';
		?>
		<option class="i<?php echo $current_tab;?>" value="<?php echo $value['url']; ?>" <?php echo $newtab;?>><?php echo $value['naviname']; ?></option>
			<?php if (!empty($value['children'])) :?>
                <?php foreach ($value['children'] as $row){
                        echo '<option value="'.Url::sort($row['sid']).'">'.$row['sortname'].'</option>';
                }?>
            <?php endif;?>

            <?php if (!empty($value['childnavi'])) :?>
                <?php foreach ($value['childnavi'] as $row){
                        $newtab = $row['newtab'] == 'y' ? 'target="_blank"' : '';
                        echo '<option value="' . $row['url'] . "\" $newtab >" . $row['naviname'].'</option>';
                }?>
            <?php endif;?>
	<?php endforeach; ?>
	</select>
<?php }?>
<?php
//blog：分类导航
function navi_sort(){
	global $CACHE;
	$sort_cache = $CACHE->readCache('sort'); ?>
	<ul>
	<?php
	foreach($sort_cache as $value):
		if ($value['pid'] != 0) continue;
	?>
	<li class="cat-item cat-item current-cat">
	<a href="<?php echo Url::sort($value['sid']); ?>" title="查看<?php echo $value['sortname']; ?>下的所有文章"><?php echo $value['sortname']; ?>(<?php echo $value['lognum'] ?>)</a>
	<?php if (!empty($value['children'])): ?>
		<ul class="children">
		<?php
		$children = $value['children'];
		foreach ($children as $key):
			$value = $sort_cache[$key];
		?>
		<li class="cat-item cat-item">
			<a href="<?php echo Url::sort($value['sid']); ?>" title="查看<?php echo $value['sortname']; ?>下的所有文章"><?php echo $value['sortname']; ?>(<?php echo $value['lognum'] ?>)</a>
		</li>
		<?php endforeach; ?>
		</ul>
    </li>
	<?php endif; ?>
	<?php endforeach; ?>
	</ul>
<?php }?>
<?php
//blog：置顶
function topflg($top, $sortop='n', $sortid=null){
    if(blog_tool_ishome()) {
       echo $top == 'y' ? "<img src=\"".TEMPLATE_URL."/images/top.png\" title=\"首页置顶文章\" /> " : '';
    } elseif($sortid){
       echo $sortop == 'y' ? "<img src=\"".TEMPLATE_URL."/images/sortop.png\" title=\"分类置顶文章\" /> " : '';
    }
}
?>
<?php
//blog：编辑
function editflg($logid,$author){
	$editflg = ROLE == 'admin' || $author == UID ? '<span><i class="icon-edit icon-large"></i> <a href="'.BLOG_URL.'admin/write_log.php?action=edit&gid='.$logid.'" class="post-edit-link" target="_blank">编辑</a></span>' : '';
	echo $editflg;
}
?>
<?php
//blog：分类
function blog_sort($blogid){
	global $CACHE; 
	$log_cache_sort = $CACHE->readCache('logsort');
	?>
	<?php if(!empty($log_cache_sort[$blogid])): ?>
    <a href="<?php echo Url::sort($log_cache_sort[$blogid]['id']); ?>" rel="category tag"><?php echo $log_cache_sort[$blogid]['name']; ?></a>
	<?php endif;?>
<?php }?>
<?php
//blog：日志标签
function blog_tag($blogid){
	global $CACHE;
	$log_cache_tags = $CACHE->readCache('logtags');
	if (!empty($log_cache_tags[$blogid])){
		foreach ($log_cache_tags[$blogid] as $key=>$value){
			$tag .= "<a href=\"".Url::tag($value['tagurl'])."\" class=\"tag".$key."\">".$value['tagname'].'</a>';
		}
	}else {
		echo '<a class=\"tag".$key."\>此文暂无标签</a>';
	}
}
?>
<?php
//blog：文章作者
function blog_author($uid){
	global $CACHE;
	$user_cache = $CACHE->readCache('user');
	$author = $user_cache[$uid]['name'];
	$mail = $user_cache[$uid]['mail'];
	$des = $user_cache[$uid]['des'];
	$title = !empty($mail) || !empty($des) ? "title=\"$des $mail\"" : '';
	echo '<b href="'.Url::author($uid)."\" class='upp' $title>$author</b>";
}
?>
<?php
//blog：相邻文章
function neighbor_log($neighborLog){
	extract($neighborLog);?>
	<?php if($prevLog):?>
	<li class="previous"><a href="<?php echo Url::log($prevLog['gid']) ?>" title="<?php echo $prevLog['title'];?>">上一篇</a></li>
	<?php endif;?>
	
	<?php if($nextLog):?>
		<li class="next"> <a href="<?php echo Url::log($nextLog['gid']) ?>" title="<?php echo $nextLog['title'];?>">下一篇</a></li>
	<?php endif;?>
<?php }?>
<?php
//blog：评论列表
function blog_comments($comments,$comnum){
    extract($comments);
    if($commentStacks): ?>
   <h1>评论</h1>
   <div class="content">
    <div id="comments">
    <a name="comments" class="comment-top"></a>
<ol class="commentlist">
	<?php endif; ?>
	<?php
	$isGravatar = Option::get('isgravatar');
	foreach($commentStacks as $cid):
    $comment = $comments[$cid];
	$comment['content'] = preg_replace("|\[em_(\d+)\]|i",'<img src="' . BLOG_URL. 'admin/editor/plugins/emoticons/images/$1.gif" />
	',$comment['content']);
	$comment['poster'] = $comment['url'] ? '
	<a href="'.$comment['url'].'" target="_blank" class="upps">'.$comment['poster'].'</a>' : $comment['poster'];
	?>
	<a name="<?php echo $comment['cid']; ?>"></a>
	<li id="li-comment-<?php echo $comment['cid']; ?>" class="comment even thread-even depth-<?php echo $comment['cid']; ?>
	">
	<div id="comment-<?php echo $comment['cid']; ?>
		" class="comment-body">
		<div class="comment-author">
			<?php if($isGravatar == 'y'): ?>
			<img alt="" src="<?php echo getGravatar($comment['mail']); ?>" class="avatar avatar-32 photo" width="32" height="32" /><?php endif; ?>
		</div>
		<div class="comment-head">
			<span class="name"><?php echo $comment['poster']; ?>
			</span>
			<p>
				<?php echo $comment['content']; ?>
			</p>
			<div class="post-reply">
				<a class="comment-reply-link" href="#comment-<?php echo $comment['cid']; ?>" onclick="commentReply(<?php echo $comment['cid']; ?>,this)">回复</a>
			</div>
			<div class="date">
				<?php echo $comment['date']; ?>
			</div>
		</div>
		<a>
		<time datetime="<?php echo $comment['date']; ?>">
		</time>
		</a>
	</div>
	<?php blog_comments_children($comments, $comment['children']); ?>
	</li>
	<?php endforeach; ?>
</ol>
<?php }?>
<?php
//blog：子评论列表
function blog_comments_children($comments, $children){
	$isGravatar = Option::get('isgravatar');
	foreach($children as $child) {
	$comment = $comments[$child];
	$comment['content'] = preg_replace("|\[em_(\d+)\]|i",'<img src="' . BLOG_URL. 'admin/editor/plugins/emoticons/images/$1.gif" />',$comment['content']);
	$comment['poster'] = $comment['url'] ? '<a href="'.$comment['url'].'" target="_blank" class="upps">'.$comment['poster'].'</a>' : $comment['poster'];
	?>
<ul class="children">
	<li class="comment byuser comment-author-lpj241 bypostauthor odd alt depth-2" id="li-comment-<?php echo $comment['cid']; ?>">
	<a name="<?php echo $comment['cid']; ?>"></a>
	<div id="comment-<?php echo $comment['cid']; ?>
		" class="comment-body">
		<div class="comment-author">
<?php if($isGravatar == 'y'): ?>
			<img alt='' src='<?php echo getGravatar($comment['mail']); ?>'  /><?php endif; ?>
		</div>
		<div class="comment-head">
			<span class="name"><?php echo $comment['poster']; ?>
			</span>
			<span class="num"></span>
			<p>
			</p>
			<p>
				<?php echo $comment['content']; ?>
			</p>
			<p>
			</p>
			<div class="post-reply">
				<a class="comment-reply-link" href="#comment-<?php echo $comment['cid']; ?>" onclick="commentReply(<?php echo $comment['cid']; ?>,this)">回复</a>
			</div>
			<div class="date">
				<?php echo $comment['date']; ?>
			</div>
		</div>
	</div>
	</li>
	<?php blog_comments_children($comments, $comment['children']);?>
</ul><!-- .children -->
	<?php } ?>
<?php }?>
<?php
//blog：发表评论表单
function blog_comments_post($logid,$ckname,$ckmail,$ckurl,$verifyCode,$allow_remark){
	if($allow_remark == 'y'): ?>
    <div id="comment-place">
    <div class="comment-post" id="comment-post">
<div class="content">

		<h3 class="clearfix">
        
        <small><a name="respond"></a><span class="cancel-reply" id="cancel-reply" style="display: none;"><a href="javascript:void(0);" onclick="cancelReply()" id="cancel-comment-reply-link">点击这里取消回复。</a></span></small></h3>
		<form action="<?php echo BLOG_URL; ?>index.php?action=addcom" method="post" id="commentform" class="comment-form">
									<div id="commentform-error" class="alert hidden"></div>	
                                    			<?php if(ROLE == 'visitor'): ?>
<div id="author_info">
                                    <p><input type="text" size="15" name="comname" id="author" placeholder="尊姓大名啊"></p>
<p><input type="text" name="commail" id="email" placeholder="这里是你的邮箱地址"   size="15"></p>
<p><input type="text" name="comurl" id="url" placeholder="这里是你的网站地址"   size="15">
</p></div>
<?php else:
          $CACHE = Cache::getInstance();
	       $user_cache = $CACHE->readCache('user');
        ?>
        <p class="title welcome">已经登录。<a href="<?php echo BLOG_URL; ?>admin/?action=logout" title="登出此帐户">登出？</a></p>	
		<?php endif; ?>
        <div id="author_textarea">
				<textarea id="comment" placeholder="赶快发表你的见解吧！" name="comment" class="textarea" cols="45" rows="5" tabindex="4" aria-required="true" style="width:100%;"></textarea>	
                </div>					
                <p>
<input name="submit" type="submit" id="submit" value="确认提交"  class="submit"/>
										<input type="hidden" name="gid" value="<?php echo $logid; ?>" id="comment_post_ID"/>
			<input type="hidden" name="pid" id="comment-pid" value="0" />
						</p>
			</form>
	</div>
</div>
	</div>
	<?php endif; ?>
    <?php }?>
<?php
//blog-tool:判断是否是首页
function blog_tool_ishome(){
    if (BLOG_URL . trim(Dispatcher::setPath(), '/') == BLOG_URL){
        return true;
    } else {
        return FALSE;
    }
}
?>

<?php
//获取附件第一张图片
function getThumbnail($blogid){
    $db = MySql::getInstance();
    $sql = "SELECT * FROM ".DB_PREFIX."attachment WHERE blogid=".$blogid." AND (`filepath` LIKE '%jpg' OR `filepath` LIKE '%gif' OR `filepath` LIKE '%png') ORDER BY `aid` ASC LIMIT 0,1";
    //die($sql);
    $imgs = $db->query($sql);
    $img_path = "";
    while($row = $db->fetch_array($imgs)){
         $img_path .= BLOG_URL.substr($row['filepath'],3,strlen($row['filepath']));
    }
    return $img_path;
}
?>
<?php
function handlearticledes($des) {
	$str = preg_replace("/(<\/?)(\w+)([^>]*>)/e",'',$des);
	$str = preg_replace("/阅读全文>>/",'',$str);
	$str = strip_tags($str,""); 
    $str = ereg_replace("\t","",$str); 
    $str = ereg_replace("\r\n","",$str); 
    $str = ereg_replace("\r","",$str); 
    $str = ereg_replace("\n","",$str); 
    $str = ereg_replace(" "," ",$str); 
	return mb_substr($str,0,200,'utf8').'...';
}
?>
<?php
	function getTopLogs($num) {
    $db = MySql::getInstance();
	$ver= Option::EMLOG_VERSION;
   if($ver == '5.3.0') 
{
 $sql = "SELECT gid,title,content,date FROM ".DB_PREFIX."blog WHERE type='blog' and top='y' or sortop='y' ORDER BY `top` DESC ,`date` DESC LIMIT 0,$num";}
else
{
$sql = "SELECT gid,title,content,date FROM ".DB_PREFIX."blog WHERE type='blog' and top='y'  ORDER BY `top` DESC ,`date` DESC LIMIT 0,2";
}
        $list = $db->query($sql);
        while($row = $db->fetch_array($list)){ ?>
<div class="top_post"><div class="title">置 顶</div><article class="ulist"><h2><a href="<?php echo Url::log($row['gid']); ?>" title="<?php echo $row['log_description']; ?>" target="_blank"><i class="icon-eject icon-large"></i><?php echo $row['title']; ?></a><span><?php echo gmdate('Y-n-j', $row['date']); ?></span></h2></article></div>
		<?php }?>
 <?php } ?>

<?php
	function related_logs($logData = array())
	{
	$configfile = EMLOG_ROOT.'/content/templates/zanblog3/config.php';
	if (is_file($configfile)) {
	require $configfile;
	}else{
	    $related_log_type = 'sort';//相关日志类型，sort为分类，tag为日志；
	    $related_log_sort = 'rand';//排列方式，views_desc 为点击数（降序）comnum_desc 为评论数（降序） rand 为随机 views_asc 为点击数（升序）comnum_asc 为评论数（升序）
	    $related_log_num = '10'; //显示文章数，排版需要，只能为10
	    $related_inrss = 'y'; //是否显示在rss订阅中，y为是，其它值为否
	    }
	    global $value;
	    $DB = MySql::getInstance();
	    $CACHE = Cache::getInstance();
	    extract($logData);
	    if($value)
	    {
	        $logid = $value['id'];
	        $sortid = $value['sortid'];
	        global $abstract;
	    }
	    $sql = "SELECT gid,title FROM ".DB_PREFIX."blog WHERE hide='n' AND type='blog'";
	    if($related_log_type == 'tag')
	    {
	        $log_cache_tags = $CACHE->readCache('logtags');
	        $Tag_Model = new Tag_Model();
	        $related_log_id_str = '0';
	        foreach($log_cache_tags[$logid] as $key => $val)
	        {
	            $related_log_id_str .= ','.$Tag_Model->getTagByName($val['tagname']);
	        }
	        $sql .= " AND gid!=$logid AND gid IN ($related_log_id_str)";
	    }else{
	        $sql .= " AND gid!=$logid AND sortid=$sortid";
	    }
	    switch ($related_log_sort)
	    {
	        case 'views_desc':
	        {
	            $sql .= " ORDER BY views DESC";
	            break;
	        }
	        case 'views_asc':
			{
	            $sql .= " ORDER BY views ASC";
	            break;
	        }
	        case 'comnum_desc':
	        {
	            $sql .= " ORDER BY comnum DESC";
	            break;
	        }
	        case 'comnum_asc':
	        {
	            $sql .= " ORDER BY comnum ASC";
	            break;
	        }
	        case 'rand':
	        {
	            $sql .= " ORDER BY rand()";
	            break;
	        }
	    }
	    $sql .= " LIMIT 0,$related_log_num";
	    $related_logs = array();
	    $query = $DB->query($sql);
	    while($row = $DB->fetch_array($query))
	    {
	        $row['gid'] = intval($row['gid']);
	        $row['title'] = htmlspecialchars($row['title']);
	        $related_logs[] = $row;
	    }
	    $out = '';
	    if(!empty($related_logs))
	    {
	        foreach($related_logs as $val)
	        {
	            $out .= "<li><a href=\"".Url::log($val['gid'])."\">{$val['title']}</a></li>";
	        }
	    }
	    if(!empty($value['content']))
	    {
	        if($related_inrss == 'y')
	        {
	            $abstract .= $out;
	        }
	    }else{
	        echo $out;
	    }
	}	 
?>
<?php
function timer_start() {
  global $timestart;
  $mtime = explode( ' ', microtime() );
  $timestart = $mtime[1] + $mtime[0];
  return true;
}
timer_start();
 
function timer_stop( $display = 0, $precision = 3 ) {
  global $timestart, $timeend;
  $mtime = explode( ' ', microtime() );
  $timeend = $mtime[1] + $mtime[0];
  $timetotal = $timeend - $timestart;
  $r = number_format( $timetotal, $precision );
  if ( $display )
    echo $r;
  return $r;
}
?>

<?php
//文章关键词
function log_key_words($blogid){
	global $CACHE;
	$log_cache_tags = $CACHE->readCache('logtags');
	if (!empty($log_cache_tags[$blogid])){
		$tag = '';
		foreach ($log_cache_tags[$blogid] as $value){
			$tag .="".$value['tagname'].',';
		}
		echo substr($tag,0,-1);
	}
}
?>

<?php
//随便看看
function sbkk_logs() {
$db = MySql::getInstance();
$sql = "SELECT gid FROM ".DB_PREFIX."blog WHERE type='blog' and hide='n' ORDER BY rand() LIMIT 0,1";
$sbkk_logs_list = $db->query($sql);
while($row = $db->fetch_array($sbkk_logs_list)){ 
echo Url::log($row['gid']);}
}?>

<?php
//blog：small分类导航
function blog_small_sort(){
	global $CACHE;
	$sort_cache = $CACHE->readCache('sort'); ?>
	<select>
	<option value="">请选择</option>
	<option value="<?php echo BLOG_URL; ?>">返回首页</option>
	<?php
	foreach($sort_cache as $value):
		if ($value['pid'] != 0) continue;
	?>
	<option value="<?php echo Url::sort($value['sid']); ?>"><?php echo $value['sortname']; ?></option>
	<?php endforeach; ?>
	</select>
<?php }?>