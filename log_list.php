<?php 
/**
 * 站点首页模板
 */
if(!defined('EMLOG_ROOT')) {exit('error!');} 
?>
<!-- main container -->
<div class="container">
	<div class="article-list" id="article-list">
	<?php 
	if (!empty($logs)):
	foreach($logs as $value): 
	?>
		<article class="article">
			<h1><a href="<?php echo $value['log_url']; ?>" title="详细阅读《<?php echo $value['log_title']; ?>》" alt="<?php echo $value['log_title']; ?>"><?php echo $value['log_title']; ?></a></h1>
                
			<div class="content">
			<p><?php echo subString(strip_tags($value['content']),0,_g('dis_num'));?></p>
			</div>
                
			<div class="article-info">
			<i class="fa fa-calendar"></i> <?php echo gmdate('Y-n-j', $value['date']); ?> &nbsp;<!-- <i class="fa fa-map-marker"></i> 
			<?php // echo $value['comnum']; ?>条评论    -->            
			</div>
			
			<div class="readmore"><a href="<?php echo $value['log_url']; ?>" title="详细阅读《<?php echo $value['log_title']; ?>》" alt="<?php echo $value['log_title']; ?>">+ 阅读全文丨<?php echo $value['views']; ?>℃</a>
			</div>            
		
		</article>
                
<?php 
endforeach;
else:
?>
		<article class="article">
		<h1>Sorry, 没有文章</h1>
		
		<div class="aside">
                   没有搜索的相关文章
		</div>  
        </article>
	
	<?php endif;?>
			
	<div class="pagenavi"><?php echo $page_url;?></div>
	
    
	</div>
	<?php include View::getView('side');?>
	
 <?php include View::getView('footer');?>