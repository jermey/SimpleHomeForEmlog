<?php 

/**

 * 微语部分

 */

if(!defined('EMLOG_ROOT')) {exit('error!');} 

?>

<div class="container">
	<div class="article-list" id="article-list">
		<section class="comments">
		<h1>神奇的微语</h1>
		<div class="content">
			<div id="comments">
				<ol class="commentlist">
					<?php 

    foreach($tws as $val):

    $author = $user_cache[$val['author']]['name'];

    $avatar = empty($user_cache[$val['author']]['avatar']) ? 

                BLOG_URL . 'admin/views/images/avatar.jpg' : 

                BLOG_URL . $user_cache[$val['author']]['avatar'];

    $tid = (int)$val['id'];

    $img = empty($val['img']) ? "" : '<a title="查看图片" href="'.BLOG_URL.str_replace('thum-', '', $val['img']).'" target="_blank">

					<img style="border: 1px solid #EFEFEF;" src="'.BLOG_URL.$val['img'].'"/></a>';

    ?>

					<li id="li-comment-11" class="comment even thread-even depth-11	">

					<div id="comment-11 " class="comment-body">

						<div class="comment-author">

							<img alt="" src="<?php echo $avatar; ?>" class="avatar avatar-32 photo" width="32" height="32">

						</div>

						<div class="comment-head">

							<span class="name">

							<?php echo $author; ?>

							</span>

							<p>

								<?php echo $val['t'].'<br/>

			'.$img;?>

							</p>

							<div class="post-reply">

							</div>

							<div class="date">

								<?php echo $val['date'];?>

							</div>

						</div>

						<a>

						<time datetime="<?php echo $val['date'];?>">

						</time>

						</a>

					</div>

					</li>

                    <?php endforeach;?>

				</ol>

			</div>

		</div>

		</section>

		<div class="pagenavi">

			<?php echo $page_url;?>

		</div>
	</div>
		<?php include View::getView('side');?>

		<?php include View::getView('footer');?>