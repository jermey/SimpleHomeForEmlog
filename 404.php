<?php
 include View::getView('header');
?>
    <div class="container">
    	<div class="article-list" >
			<article class="article">
				<h1>很抱歉，没有找到你想要的内容。</h1>	
                <div class="content">
				<embed type="application/x-shockwave-flash" width="600" height="400" src="<?php echo TEMPLATE_URL; ?>404.swf" wmode="transparent" quality="high" scale="noborder" flashvars="width=600&height=400" allowscriptaccess="sameDomain" align="L">
					<p>你可以先玩一下游戏，或者<a title="返回首页" href="<?php echo BLOG_URL; ?>" >返回首页</a></p>
				</div>
				
                <div class="article-info">
                </div>
				
                <div class="readmore"><a title="返回首页" href="<?php echo BLOG_URL; ?>" >返回首页</a></div>            
			</article>
		</div>
	<?php include View::getView('side');?>
	<?php include View::getView('footer');?>